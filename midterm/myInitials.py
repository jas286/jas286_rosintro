import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import sys
import copy
import numpy as np

from math import pi, sin, cos

# Tau for declaring angles
tau = 2 * pi

# Movement length scale
scale = 1.0

# Initialize ROS Node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("first_initial", anonymous=True)

# Create the Move Group
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Go to the starting postion for the J initial via joint angles
def go_to_J_start():
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0.993
    joint_goal[1] = -2.157
    joint_goal[2] = -1.048
    joint_goal[3] = 0.064
    joint_goal[4] = 0.576
    joint_goal[5] = 2.356

    move_group.go(joint_goal, wait=True)
    move_group.stop()

# Go to the starting postion for the A and S initials via joint angles
def go_to_A_and_S_start():
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0.993
    joint_goal[1] = -2.157
    joint_goal[2] = -1.4
    joint_goal[3] = 0.064
    joint_goal[4] = 0.576
    joint_goal[5] = 2.356

    move_group.go(joint_goal, wait=True)
    move_group.stop()


# Plan the swoop of the J
def plan_J_swoop():
    waypoints = []
    wpose = move_group.get_current_pose().pose

    z = wpose.position.z
    y = wpose.position.y + 0.2 * scale

    # Swoop through a full semicircle
    thetas = np.linspace(tau / 40, tau / 2, 10)

    for theta in thetas:
        wpose.position.z = z - sin(theta) * 0.2 * scale
        wpose.position.y = y - cos(theta) * 0.2 * scale
        waypoints.append(copy.deepcopy(wpose))

    # Return the computed waypoints and current pose of the robot
    return waypoints, wpose


# Plan a path to draw my first initial: J
def plan_J_path():
    waypoints, wpose = plan_J_swoop()

    # Move up for the stem of the J
    wpose.position.z += 0.25 * scale
    waypoints.append(copy.deepcopy(wpose))


    # Move right/left to create the top of the J
    wpose.position.y += 0.15 * scale
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y -= 0.3 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Compute the plan
    (plan, fraction) = move_group.compute_cartesian_path(
                waypoints, 0.01, 0.0)


    # return the plan
    return plan


# Plan the path to draw my second initial: A
def plan_A_path():
    waypoints = []
    wpose = move_group.get_current_pose().pose

    wpose.position.z += 0.25*scale
    wpose.position.y += 0.1*scale
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.z -= 0.25*scale
    wpose.position.y += 0.1*scale
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.z += 0.125*scale
    wpose.position.y -= 0.15*scale
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y += 0.1*scale
    waypoints.append(copy.deepcopy(wpose))

    # Compute the plan
    (plan, fraction) = move_group.compute_cartesian_path(
                waypoints, 0.01, 0.0)

    return plan


# Plan the path to draw my third initial: S
def plan_S_path():
    waypoints = []
    wpose = move_group.get_current_pose().pose

    z = wpose.position.z
    y = wpose.position.y + 0.1 * scale

    # Bottom swoop of the S
    thetas = np.linspace(tau / 40, 3*tau / 4, 10)
    for theta in thetas:
        wpose.position.z = z - sin(theta) * 0.1 * scale
        wpose.position.y = y - cos(theta) * 0.1 * scale
        waypoints.append(copy.deepcopy(wpose))


    z = wpose.position.z + 0.1 * scale
    y = wpose.position.y
    
    # Top swoop of the S
    for theta in thetas:
        theta += 3*tau/4
        theta = -theta
        wpose.position.z = z - sin(theta) * 0.1 * scale
        wpose.position.y = y - cos(theta) * 0.1 * scale
        waypoints.append(copy.deepcopy(wpose))

    # Compute the plan
    (plan, fraction) = move_group.compute_cartesian_path(
                waypoints, 0.01, 0.0)

    return plan


# Execute the J initial plan
go_to_J_start()
move_group.execute(plan_J_path(), wait=True)

# Execute the A initial plan
go_to_A_and_S_start()
move_group.execute(plan_A_path(), wait=True)

# Execute the S initial plan
go_to_A_and_S_start()
move_group.execute(plan_S_path(), wait=True)

