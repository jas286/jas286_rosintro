import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import sys
import copy


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("first_initial", anonymous=True)

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

robot = moveit_commander.RobotCommander()

print("============ Printing robot state")
print(robot.get_current_state())
print("")
