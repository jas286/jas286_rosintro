import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import sys
import copy


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("first_initial", anonymous=True)

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

def go_to_start():
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0.993
    joint_goal[1] = -2.157
    joint_goal[2] = -1.048
    joint_goal[3] = 0.064
    joint_goal[4] = 0.576
    joint_goal[5] = 2.356

    move_group.go(joint_goal, wait=True)
    move_group.stop()

go_to_start()
