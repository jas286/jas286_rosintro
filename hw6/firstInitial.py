import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import sys
import copy
import numpy as np

from math import pi, sin, cos

# Tau for declaring angles
tau = 2 * pi

# Movement length scale
scale = 1.2

# Initialize ROS Node
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("first_initial", anonymous=True)

# Create the Move Group
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

# Go to the starting postion via joint angles
def go_to_start():
    joint_goal = move_group.get_current_joint_values()
    joint_goal[0] = 0.993
    joint_goal[1] = -2.157
    joint_goal[2] = -1.048
    joint_goal[3] = 0.064
    joint_goal[4] = 0.576
    joint_goal[5] = 2.356

    move_group.go(joint_goal, wait=True)
    move_group.stop()


# Plan the swoop of the J
def plan_J_swoop():
    waypoints = []
    wpose = move_group.get_current_pose().pose

    z = wpose.position.z
    y = wpose.position.y + 0.2 * scale

    # Swoop through a full semicircle
    thetas = np.linspace(tau / 40, tau / 2, 10)

    for theta in thetas:
        wpose.position.z = z - sin(theta) * 0.2 * scale
        wpose.position.y = y - cos(theta) * 0.2 * scale
        waypoints.append(copy.deepcopy(wpose))

    # Return the computed waypoints and current pose of the robot
    return waypoints, wpose


# Plan a path to draw my first initial: J
def plan_J_path():
    waypoints, wpose = plan_J_swoop()

    # Move up for the stem of the J
    wpose.position.z += 0.25 * scale
    waypoints.append(copy.deepcopy(wpose))


    # Move right/left to create the top of the J
    wpose.position.y += 0.15 * scale
    waypoints.append(copy.deepcopy(wpose))

    wpose.position.y -= 0.3 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Compute the plan
    (plan, fraction) = move_group.compute_cartesian_path(
                waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
                )  # jump_threshold


    # return the plan
    return plan


# Move the robot to the starting position
go_to_start()

# Compute the J plan
j_plan = plan_J_path()

# Execute the plan
move_group.execute(j_plan, wait=True)

