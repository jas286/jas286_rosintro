#!/usr/bin/bash

rosservice call /turtle1/teleport_relative -- '-2.8' '0.0'
rosservice call /clear
rosservice call /turtle1/set_pen 0 255 0 4 off

./turtleletter.sh

rosservice call /spawn 6.5 3 0 ""

rosservice call /turtle2/set_pen 255 0 0 4 off

rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist \
	-- '[8.5, 0.0, 0.0]' '[0.0, 0.0, 6.5]'
