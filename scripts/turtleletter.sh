#!/usr/bin/bash 

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.555]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.555]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 3.12]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 3.12]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.555]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[4.5, 0.0, 0.0]' '[0.0, 0.0, -3.5]'
